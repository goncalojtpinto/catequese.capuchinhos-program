package cat.cap.cms.utils;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author Gonçalo Pinto (1180987)
 */
public class UtilsTest {

    /**
     * Test of next method, of class Utils.
     */
    @Test
    public void testNext() {
        System.out.println("next");

        int result = Utils.next(4);
        assertEquals(5, result);
    }

    /**
     * Test of previous method, of class Utils.
     */
    @Test
    public void testPrevious() {
        System.out.println("previous");

        int result = Utils.previous(4);
        assertEquals(3, result);
    }

    /**
     * Test of smallerThan method, of class Utils.
     */
    @Test
    public void testSmallerThanSmaller() {
        System.out.println("smallerThan - Smaller");

        boolean result = Utils.smallerThan(4, 5);
        assertEquals(true, result);
    }
    
    /**
     * Test of smallerThan method, of class Utils.
     */
    @Test
    public void testSmallerThanEqual() {
        System.out.println("smallerThan - Equal");

        boolean result = Utils.smallerThan(5, 5);
        assertEquals(false, result);
    }
    
    /**
     * Test of smallerThan method, of class Utils.
     */
    @Test
    public void testSmallerThanBigger() {
        System.out.println("smallerThan - Bigger");

        boolean result = Utils.smallerThan(6, 5);
        assertEquals(false, result);
    }

    /**
     * Test of smallerOrEqual method, of class Utils.
     */
    @Test
    public void testSmallerOrEqualSmaller() {
        System.out.println("smallerOrEqual - Smaller");

        boolean result = Utils.smallerOrEqual(4, 5);
        assertEquals(true, result);
    }
    
    /**
     * Test of smallerOrEqual method, of class Utils.
     */
    @Test
    public void testSmallerOrEqualEqual() {
        System.out.println("smallerOrEqual - Equal");

        boolean result = Utils.smallerOrEqual(5, 5);
        assertEquals(true, result);
    }
    
    /**
     * Test of smallerOrEqual method, of class Utils.
     */
    @Test
    public void testSmallerOrEqualBigger() {
        System.out.println("smallerOrEqual - Bigger");

        boolean result = Utils.smallerOrEqual(6, 5);
        assertEquals(false, result);
    }

    /**
     * Test of biggerThan method, of class Utils.
     */
    @Test
    public void testBiggerThanSmaller() {
        System.out.println("biggerThan - Smaller");

        boolean result = Utils.biggerThan(4, 5);
        assertEquals(false, result);
    }
    
    /**
     * Test of biggerThan method, of class Utils.
     */
    @Test
    public void testBiggerThanEqual() {
        System.out.println("biggerThan - Equal");

        boolean result = Utils.biggerThan(5, 5);
        assertEquals(false, result);
    }
    
    /**
     * Test of biggerThan method, of class Utils.
     */
    @Test
    public void testBiggerThanBigger() {
        System.out.println("biggerThan - Bigger");

        boolean result = Utils.biggerThan(6, 5);
        assertEquals(true, result);
    }

    /**
     * Test of biggerOrEqual method, of class Utils.
     */
    @Test
    public void testBiggerOrEqualSmaller() {
        System.out.println("biggerOrEqual - Smaller");

        boolean result = Utils.biggerOrEqual(4, 5);
        assertEquals(false, result);
    }
    
    /**
     * Test of biggerOrEqual method, of class Utils.
     */
    @Test
    public void testBiggerOrEqualEqual() {
        System.out.println("biggerOrEqual - Equal");

        boolean result = Utils.biggerOrEqual(5, 5);
        assertEquals(true, result);
    }
    
    /**
     * Test of biggerOrEqual method, of class Utils.
     */
    @Test
    public void testBiggerOrEqualBigger() {
        System.out.println("biggerOrEqual - Bigger");

        boolean result = Utils.biggerOrEqual(6, 5);
        assertEquals(true, result);
    }
}
