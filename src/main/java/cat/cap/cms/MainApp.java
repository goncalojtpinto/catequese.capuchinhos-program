package cat.cap.cms;

import cat.cap.cms.model.*;
import cat.cap.cms.utils.*;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainApp extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Scene.fxml"));
        
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");
        
        stage.setTitle("JavaFX and Maven");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
//        launch(args);
        Catequizando cat = new Catequizando("Gonçalo José Teixeira Pinto", Catequizando.GENERO_MASC, new Contactos(224640075, 939388266, "goncalojtpinto@gmail.com"), new Nascimento(new Data(2000, 03, 15), "Gondomar"), new Batismo(new Data(2000,05,10), "S. Cosme"), new Morada("Rua Patrício Gouveia 324", new CodigoPostal(4420, 254), "S. Cosme, Gondomar"), new Progenitor("João de Jesus Pinto", Progenitor.GENERO_MASC, new Contactos(224640075, 964284254, ""), "Ourives", "Casado"), new Progenitor("Maria das Dores da Silva Teixeira Pinto", Progenitor.GENERO_FEM, new Contactos(224640075, 968209349, "mdorestpinto@hotmail.com"), "Desempregada", "Casada"), new AnoLetivo(2005), "Capuchinhos", new AnoCatequetico(14, new AnoLetivo(2019)), true);
        FichaReInscricaoGenerator ficha = new FichaReInscricaoGenerator();
        ficha.generateDocxFileFromTemplate(cat);
    }

}
