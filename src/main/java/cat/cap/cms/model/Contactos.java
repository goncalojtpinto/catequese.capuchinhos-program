package cat.cap.cms.model;

import java.util.Objects;

/**
 *
 * @author Gonçalo Pinto
 */
public class Contactos {

    public long telefone;
    public long telemovel;
    public String email;

    public Contactos(long telefone, long telemovel, String email) {
        this.telefone = telefone;
        this.telemovel = telemovel;
        this.email = email;
    }

    public long getTelefone() {
        return this.telefone;
    }

    public long getTelemovel() {
        return this.telemovel;
    }

    public String getEmail() {
        return this.email;
    }

    public void setTelefone(long telefone) {
        this.telefone = telefone;
    }

    public void setTelemovel(long telemovel) {
        this.telemovel = telemovel;
    }

    public boolean setEmail(String email) {
        if (email == null) {
            return false;
        }
        this.email = email;
        return true;
    }

    @Override
    public String toString() {
        return String.format("Contactos:      " + this.telefone
                + "\n           +351 " + this.telemovel
                + "\n           " + this.email);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (int) (this.telefone ^ (this.telefone >>> 32));
        hash = 67 * hash + (int) (this.telemovel ^ (this.telemovel >>> 32));
        hash = 67 * hash + Objects.hashCode(this.email);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Contactos other = (Contactos) obj;
        if (this.telefone != other.telefone) {
            return false;
        }
        if (this.telemovel != other.telemovel) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        return true;
    }
}
