package cat.cap.cms.model;

import cat.cap.cms.utils.AnoLetivo;
import java.util.Objects;

/**
 *
 * @author Gonçalo Pinto (1180987)
 */
public class AnoCatequetico {
    
    public int ano;
    public AnoLetivo anoLetivo;

    public AnoCatequetico(int ano, AnoLetivo anoLetivo) {
        this.ano = ano;
        this.anoLetivo = anoLetivo;
    }

    public int getAno() {
        return ano;
    }

    public AnoLetivo getAnoLetivo() {
        return anoLetivo;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public void setAnoLetivo(AnoLetivo anoLetivo) {
        this.anoLetivo = anoLetivo;
    }

    @Override
    public String toString() {
        return String.format(ano + "º");
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.ano;
        hash = 53 * hash + Objects.hashCode(this.anoLetivo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AnoCatequetico other = (AnoCatequetico) obj;
        if (this.ano != other.ano) {
            return false;
        }
        if (!Objects.equals(this.anoLetivo, other.anoLetivo)) {
            return false;
        }
        return true;
    }
}
