package cat.cap.cms.model;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Gonçalo Pinto
 */
public class Sacramento {
    
    public int ano;
    public String nome;
    public Date data;
    public String local;

    public Sacramento(int ano, String nome, Date data, String local) {
        this.ano = ano;
        this.nome = nome;
        this.data = data;
        this.local = local;
    }

    public int getAno() {
        return ano;
    }

    public String getNome() {
        return nome;
    }

    public Date getData() {
        return data;
    }

    public String getLocal() {
        return local;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    @Override
    public String toString() {
        return String.format(ano + "º Ano - " + nome + ", " + data + " - " + local);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.ano;
        hash = 37 * hash + Objects.hashCode(this.nome);
        hash = 37 * hash + Objects.hashCode(this.data);
        hash = 37 * hash + Objects.hashCode(this.local);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sacramento other = (Sacramento) obj;
        if (this.ano != other.ano) {
            return false;
        }
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.local, other.local)) {
            return false;
        }
        if (!Objects.equals(this.data, other.data)) {
            return false;
        }
        return true;
    }
    
    
}
