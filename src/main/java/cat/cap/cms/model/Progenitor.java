package cat.cap.cms.model;

import java.util.Objects;

/**
 *
 * @author Gonçalo Pinto
 */
public class Progenitor extends Pessoa {

    public String profissao;
    public String estadoCivil;

    public Progenitor(String nome, String genero, Contactos contactos, String profissao, String estadoCivil) {
        super(nome, genero, contactos);
        this.profissao = profissao;
        this.estadoCivil = estadoCivil;
    }

    public String getProfissao() {
        return this.profissao;
    }

    public String getEstadoCivil() {
        return this.estadoCivil;
    }

    public boolean setProfissao(String profissao) {
        if (profissao == null) {
            return false;
        }
        this.profissao = profissao;
        return true;
    }

    public boolean setEstadoCivil(String estadoCivil) {
        if (estadoCivil == null) {
            return false;
        }
        this.estadoCivil = estadoCivil;
        return true;
    }

    @Override
    public String toString() {
        return "Progenitor{" + "profissao=" + profissao + ", estadoCivil=" + estadoCivil + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.profissao);
        hash = 67 * hash + Objects.hashCode(this.estadoCivil);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Progenitor other = (Progenitor) obj;
        if (!Objects.equals(this.profissao, other.profissao)) {
            return false;
        }
        if (!Objects.equals(this.estadoCivil, other.estadoCivil)) {
            return false;
        }
        return true;
    }
}
