package cat.cap.cms.model;

import cat.cap.cms.utils.*;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Gonçalo Pinto
 */
public class Catequizando extends Pessoa {

    public Nascimento nascimento;
    public Batismo batismo;
    public Morada morada;

    public Progenitor pai;
    public Progenitor mae;

    public AnoLetivo ano1Inscricao;
    public String centro1Inscricao;

    public AnoCatequetico anoAtual;
    public boolean inscrito;

    public Catequizando(String nome, String genero, Contactos contactos, Nascimento nascimento, Batismo batismo, Morada morada, Progenitor pai, Progenitor mae, AnoLetivo ano1Inscricao, String centro1Inscricao, AnoCatequetico anoAtual, boolean inscrito) {
        super(nome, genero, contactos);
        this.nascimento = nascimento;
        this.batismo = batismo;
        this.morada = morada;
        this.pai = pai;
        this.mae = mae;
        this.ano1Inscricao = ano1Inscricao;
        this.centro1Inscricao = centro1Inscricao;
        this.anoAtual = anoAtual;
        this.inscrito = inscrito;
    }

    public Nascimento getNascimento() {
        return this.nascimento;
    }

    public Batismo getBatismo() {
        return this.batismo;
    }

    public Morada getMorada() {
        return this.morada;
    }

    public Progenitor getPai() {
        return this.pai;
    }

    public Progenitor getMae() {
        return this.mae;
    }

    public AnoLetivo getAno1Inscricao() {
        return this.ano1Inscricao;
    }

    public String getCentro1Inscricao() {
        return this.centro1Inscricao;
    }

    public AnoCatequetico getAnoAtual() {
        return this.anoAtual;
    }

    public boolean isInscrito() {
        return this.inscrito;
    }

    public void setNascimento(Nascimento nascimento) {
        this.nascimento = nascimento;
    }

    public void setBatismo(Batismo batismo) {
        this.batismo = batismo;
    }

    public void setMorada(Morada morada) {
        this.morada = morada;
    }

    public void setPai(Progenitor pai) {
        this.pai = pai;
    }

    public void setMae(Progenitor mae) {
        this.mae = mae;
    }

    public void setAno1Inscricao(AnoLetivo ano1Inscricao) {
        this.ano1Inscricao = ano1Inscricao;
    }

    public void setCentro1Inscricao(String centro1Inscricao) {
        this.centro1Inscricao = centro1Inscricao;
    }

    public void setAnoAtual(AnoCatequetico anoAtual) {
        this.anoAtual = anoAtual;
    }

    public void setInscrito(boolean inscrito) {
        this.inscrito = inscrito;
    }

    @Override
    public String toString() {
        return String.format(super.toString());
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.nascimento);
        hash = 67 * hash + Objects.hashCode(this.batismo);
        hash = 67 * hash + Objects.hashCode(this.morada);
        hash = 67 * hash + Objects.hashCode(this.pai);
        hash = 67 * hash + Objects.hashCode(this.mae);
        hash = 67 * hash + Objects.hashCode(this.ano1Inscricao);
        hash = 67 * hash + Objects.hashCode(this.centro1Inscricao);
        hash = 67 * hash + (this.inscrito ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Catequizando other = (Catequizando) obj;
        if (this.inscrito != other.inscrito) {
            return false;
        }
        if (!Objects.equals(this.centro1Inscricao, other.centro1Inscricao)) {
            return false;
        }
        if (!Objects.equals(this.nascimento, other.nascimento)) {
            return false;
        }
        if (!Objects.equals(this.batismo, other.batismo)) {
            return false;
        }
        if (!Objects.equals(this.morada, other.morada)) {
            return false;
        }
        if (!Objects.equals(this.pai, other.pai)) {
            return false;
        }
        if (!Objects.equals(this.mae, other.mae)) {
            return false;
        }
        if (!Objects.equals(this.ano1Inscricao, other.ano1Inscricao)) {
            return false;
        }
        return true;
    }
}
