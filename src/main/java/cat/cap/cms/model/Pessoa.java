package cat.cap.cms.model;

import java.util.Objects;

/**
 *
 * @author Gonçalo Pinto
 */
public class Pessoa {

    public String nome;
    public String genero;
    public Contactos contactos;

    public static final String GENERO_MASC = "Masculino";
    public static final String GENERO_FEM = "Feminino";

    public Pessoa(String nome, String genero, Contactos contactos) {
        this.nome = nome;
        this.genero = genero;
        this.contactos = contactos;
    }

    public String getNome() {
        return this.nome;
    }

    public String getGenero() {
        return this.genero;
    }

    public Contactos getContactos() {
        return this.contactos;
    }

    public static String getGENERO_MASC() {
        return GENERO_MASC;
    }

    public static String getGENERO_FEM() {
        return GENERO_FEM;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setContactos(Contactos contactos) {
        this.contactos = contactos;
    }

    @Override
    public String toString() {
        return String.format(nome + ", contactos=" + contactos);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.nome);
        hash = 13 * hash + Objects.hashCode(this.genero);
        hash = 13 * hash + Objects.hashCode(this.contactos);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pessoa other = (Pessoa) obj;
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.genero, other.genero)) {
            return false;
        }
        if (!Objects.equals(this.contactos, other.contactos)) {
            return false;
        }
        return true;
    }
}
