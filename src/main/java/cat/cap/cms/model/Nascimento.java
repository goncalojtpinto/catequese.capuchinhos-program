package cat.cap.cms.model;

import cat.cap.cms.utils.Data;
import java.util.Objects;

/**
 *
 * @author Gonçalo Pinto
 */
public class Nascimento {

    public Data data;
    public String local;

    public Nascimento(Data data, String local) {
        this.data = data;
        this.local = local;
    }

    public Data getData() {
        return this.data;
    }

    public String getLocal() {
        return this.local;
    }

    public boolean setData(Data data) {
        if (data == null) {
            return false;
        }
        this.data = data;
        return true;
    }

    public boolean setLocal(String local) {
        if (local == null) {
            return false;
        }
        this.local = local;
        return true;
    }

    @Override
    public String toString() {
        return String.format("Nascido a " + this.data + " na Freguesia de " + this.local);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + Objects.hashCode(this.data);
        hash = 13 * hash + Objects.hashCode(this.local);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Nascimento other = (Nascimento) obj;
        if (!Objects.equals(this.local, other.local)) {
            return false;
        }
        if (!Objects.equals(this.data, other.data)) {
            return false;
        }
        return true;
    }
}
