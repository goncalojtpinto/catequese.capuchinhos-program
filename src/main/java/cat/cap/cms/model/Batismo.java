package cat.cap.cms.model;

import cat.cap.cms.utils.Data;
import java.util.Objects;

/**
 *
 * @author Gonçalo Pinto
 */
public class Batismo {

    public Data data;
    public String paroquia;

    public Batismo(Data data, String paroquia) {
        this.data = data;
        this.paroquia = paroquia;
    }

    public Data getData() {
        return this.data;
    }

    public String getParoquia() {
        return this.paroquia;
    }

    public boolean setData(Data data) {
        if (data == null) {
            return false;
        }
        this.data = data;
        return true;
    }

    public boolean setParoquia(String paroquia) {
        if (paroquia == null) {
            return false;
        }
        this.paroquia = paroquia;
        return true;
    }

    @Override
    public String toString() {
        return String.format("Batizado a " + this.data + " na Paróquia de " + this.paroquia + ".");
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + Objects.hashCode(this.data);
        hash = 19 * hash + Objects.hashCode(this.paroquia);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Batismo other = (Batismo) obj;
        if (!Objects.equals(this.paroquia, other.paroquia)) {
            return false;
        }
        if (!Objects.equals(this.data, other.data)) {
            return false;
        }
        return true;
    }
}
