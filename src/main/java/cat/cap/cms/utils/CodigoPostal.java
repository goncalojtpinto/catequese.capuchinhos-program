package cat.cap.cms.utils;

/**
 *
 * @author Gonçalo Pinto
 */
public class CodigoPostal {

    public int regiao;
    public int rua;

    public CodigoPostal(int regiao, int rua) {
        this.regiao = regiao;
        this.rua = rua;
    }

    public int getRegiao() {
        return this.regiao;
    }

    public int getRua() {
        return this.rua;
    }

    public void setRegiao(int regiao) {
        this.regiao = regiao;
    }

    public void setRua(int rua) {
        this.rua = rua;
    }

    @Override
    public String toString() {
        return String.format(regiao + "-" + rua);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.regiao;
        hash = 53 * hash + this.rua;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CodigoPostal other = (CodigoPostal) obj;
        if (this.regiao != other.regiao) {
            return false;
        }
        if (this.rua != other.rua) {
            return false;
        }
        return true;
    }
}
