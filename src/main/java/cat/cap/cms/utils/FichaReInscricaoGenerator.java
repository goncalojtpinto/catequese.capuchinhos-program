package cat.cap.cms.utils;

import cat.cap.cms.model.*;
import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import org.docx4j.Docx4J;
import org.docx4j.model.datastorage.migration.VariablePrepare;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;

/**
 *
 * @author Gonçalo Pinto
 */
public class FichaReInscricaoGenerator {

    private static final String TEMPLATE_NAME = "Ficha de Re-Inscrição.docx";
    private static final AnoLetivo ANO_LETIVO_ATUAL = new AnoLetivo(Calendar.getInstance().get(Calendar.YEAR));

    public void generateDocxFileFromTemplate(Catequizando catequizando) throws Exception {

        WordprocessingMLPackage wordMLPackage = Docx4J.load(this.getClass().getClassLoader().getResourceAsStream(TEMPLATE_NAME));
        
        MainDocumentPart documentPart = wordMLPackage.getMainDocumentPart();

        VariablePrepare.prepare(wordMLPackage);

        HashMap<String, String> variables = new HashMap<>();
        variables.put("nome", catequizando.getNome());
        variables.put("ano", String.valueOf(catequizando.getAnoAtual().getAno()));
        variables.put("nascimento", catequizando.getNascimento().toString());
        variables.put("telefone", String.valueOf(catequizando.getContactos().getTelefone()));
        variables.put("telemovel", String.valueOf(catequizando.getContactos().getTelemovel()));
        variables.put("email", catequizando.getContactos().getEmail());
        variables.put("endereco", catequizando.getMorada().getEndereco());
        variables.put("localidade", catequizando.getMorada().getLocalidade());
        variables.put("codPostal", catequizando.getMorada().getCodPostal().toString());
        variables.put("nomePai", catequizando.getPai().getNome());
        variables.put("profissaoPai", catequizando.getPai().getProfissao());
        variables.put("nomeMae", catequizando.getMae().getNome());
        variables.put("profissaoMae", catequizando.getMae().getProfissao());
        variables.put("batismo", catequizando.getBatismo().toString());
        variables.put("ano1Inscricao", catequizando.getAno1Inscricao().toString());
        variables.put("centro1Inscricao", catequizando.getCentro1Inscricao());
        variables.put("anoLetivoAtual", ANO_LETIVO_ATUAL.toString());

        documentPart.variableReplace(variables);

        File exportFile = new File("welcome.docx");
        wordMLPackage.save(exportFile);
    }
}
