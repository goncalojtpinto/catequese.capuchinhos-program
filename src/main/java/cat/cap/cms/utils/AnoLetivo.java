package cat.cap.cms.utils;

/**
 *
 * @author Gonçalo Pinto (1180987)
 */
public class AnoLetivo {
    
    public int ano1;
    public int ano2;

    public AnoLetivo(int ano1) {
        this.ano1 = ano1;
        this.ano2 = Utils.next(ano1);
    }

    public void setAno1(int ano1) {
        this.ano1 = ano1;
    }

    @Override
    public String toString() {
        return String.format(ano1 + "/" + String.valueOf(ano2).substring(2));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.ano1;
        hash = 53 * hash + this.ano2;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AnoLetivo other = (AnoLetivo) obj;
        if (this.ano1 != other.ano1) {
            return false;
        }
        if (this.ano2 != other.ano2) {
            return false;
        }
        return true;
    }
}
