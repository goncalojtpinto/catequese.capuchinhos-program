package cat.cap.cms.utils;

import java.util.Objects;

/**
 *
 * @author Gonçalo Pinto
 */
public class Morada {

    public String endereco;
    public CodigoPostal codPostal;
    public String localidade;

    public Morada(String endereco, CodigoPostal codPostal, String localidade) {
        this.endereco = endereco;
        this.codPostal = codPostal;
        this.localidade = localidade;
    }

    public String getEndereco() {
        return this.endereco;
    }

    public CodigoPostal getCodPostal() {
        return this.codPostal;
    }

    public String getLocalidade() {
        return this.localidade;
    }

    public boolean setEndereco(String endereco) {
        if (endereco == null) {
            return false;
        }
        this.endereco = endereco;
        return true;
    }

    public boolean setCodPostal(CodigoPostal codPostal) {
        if (codPostal == null) {
            return false;
        }
        this.codPostal = codPostal;
        return true;
    }

    public boolean setLocalidade(String localidade) {
        if (localidade == null) {
            return false;
        }
        this.localidade = localidade;
        return true;
    }

    @Override
    public String toString() {
        return String.format(this.endereco + " - " + this.codPostal + " " + localidade);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.endereco);
        hash = 59 * hash + Objects.hashCode(this.codPostal);
        hash = 59 * hash + Objects.hashCode(this.localidade);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Morada other = (Morada) obj;
        if (!Objects.equals(this.endereco, other.endereco)) {
            return false;
        }
        if (!Objects.equals(this.codPostal, other.codPostal)) {
            return false;
        }
        if (!Objects.equals(this.localidade, other.localidade)) {
            return false;
        }
        return true;
    }
}
