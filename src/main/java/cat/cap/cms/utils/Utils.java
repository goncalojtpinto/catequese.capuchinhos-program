package cat.cap.cms.utils;

/**
 *
 * @author Gonçalo Pinto
 */
public class Utils {

    public static int next(int num) {
        return num + 1;
    }
    
    public static int previous(int num) {
        return num - 1;
    }
    
    public static boolean smallerThan(int i, int max) {
        return i < max;
    }
    
    public static boolean smallerOrEqual(int i, int max) {
        return i <= max;
    }
    
    public static boolean biggerThan(int i, int min) {
        return i > min;
    }
    
    public static boolean biggerOrEqual(int i, int min) {
        return i >= min;
    }
}
